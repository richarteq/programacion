package semana04;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.panayotis.gnuplot.JavaPlot;

public class GNUPlot {

	
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
			
		int[]x = null;
		Double[]y = null;
		int i;
		
		//Advertencia Input
		int N;
		Scanner teclado = new Scanner( System.in );
        System.out.print( "Introduzca el tamaño del eje x: " );
        N = teclado.nextInt();
		//int N=20;
        
        x = new int[N];
		y = new Double[N];
		
		//Eje X
		for(i=0;i<N;i=i+1) {
			x[i]=i+1;
		}
		//Eje Y
		for(i=1;i<=N;i=i+1) {
			//y[i-1] = Math.sin(x[i-1]);
			y[i-1] = (double) (x[i-1]*x[i-1]);
		}
        
		//Excepción Archivo
        String archivoDatos = "datosLP321b.txt";
        PrintWriter oS;
		try {
			oS = new PrintWriter(archivoDatos);
			for(i=0;i<N;i=i+1) {
				//System.out.println(x[i] + " " + y[i]);
				oS.println(x[i] + " " + y[i]);
			}			
			oS.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//JavaPlot
		JavaPlot p = new JavaPlot();
		p.addPlot("\"/home/richart/eclipse-workspace/LP3_21b/datosLP321b.txt\" with lines");
		p.plot();

	}

}
