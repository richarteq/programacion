package semana05;

public class ProbandoParalelo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploParalelo2 obj01 = new EjemploParalelo2(4);
		EjemploParalelo2 obj02 = new EjemploParalelo2(5);
		EjemploParalelo2 obj03 = new EjemploParalelo2(7);
		
		obj01.setName("Juan");
		obj02.setName("Carlos");
		obj03.setName("Maria");
		
		System.out.println(obj01.getName() + " " + obj01.getState());
						
		obj01.start();
		obj02.start();
		obj03.start();
		System.out.println("->"+obj03.getName() + " " + obj01.getState());
		while(obj03.isAlive()==true) {
			
		}
		System.out.println(obj03.getName() + " " + obj01.getState());

	}

}
