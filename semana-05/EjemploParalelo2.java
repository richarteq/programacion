package semana05;

public class EjemploParalelo2 extends Thread {
	
	//String name;
	int tope;
	
	public EjemploParalelo2(int algo) {
		//this.name = name;
		this.tope = algo;
	}
	
	@SuppressWarnings("static-access")
	public void run() {
		
		System.out.println(this.getName() + " " + this.getState());

		for(int i=1; i<=tope; i++) {
			System.out.println("Hola soy " + this.getName() + " - " +this.getId()+ " " + i);
			
			try {
				//System.out.println(this.getName() + " " + this.getState());
				this.sleep(2000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
