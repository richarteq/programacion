package semana05;

public class ProbandoParalelo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploParalelo obj01 = new EjemploParalelo(4);
		EjemploParalelo obj02 = new EjemploParalelo(5);
		EjemploParalelo obj03 = new EjemploParalelo(7);
		
		System.out.println(obj01.getState());
		
		obj01.setName("Misti");
		obj02.setName("Misti");
		obj03.setName("Misti");
		
		obj01.start();
		obj02.start();
		obj03.start();
		
		while(obj01.isAlive()==true) {
			
		}
		System.out.println(obj01.getState());

	}

}
