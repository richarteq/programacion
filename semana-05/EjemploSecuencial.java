package semana05;

public class EjemploSecuencial {
	
	String name;
	int tope;
	
	public EjemploSecuencial(String name, int algo) {
		this.name = name;
		this.tope = algo;
	}
	
	public void run() {
		
		for(int i=1; i<=tope; i++) {
			System.out.println("Hola soy " + this.name + " " + i);
		}
	}

}
