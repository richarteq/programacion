package semana05;

public class ProbandoSecuencial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EjemploSecuencial obj01 = new EjemploSecuencial("Juan", 4);
		EjemploSecuencial obj02 = new EjemploSecuencial("Carlos", 5);
		EjemploSecuencial obj03 = new EjemploSecuencial("Maria", 7);
		
		obj01.run();
		obj02.run();
		obj03.run();

	}

}
