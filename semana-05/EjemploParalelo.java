package semana05;

public class EjemploParalelo extends Thread {
	
	//String name;
	int tope;
	
	public EjemploParalelo(int algo) {
		//this.name = name;
		this.tope = algo;
	}
	
	public void run() {
		
		for(int i=1; i<=tope; i++) {
			System.out.println("Hola soy " + this.getName() + " - " +this.getId()+ " " + i);
		}
	}

}
