package semana03;

public class Singleton2 {
	
	private Singleton2 INSTANCE = null;
	
    Singleton2(){}
    
    private void createInstance() {
        
            INSTANCE = new Singleton2();
        
    }
    
    public  Singleton2 getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    
}