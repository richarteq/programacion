package semana08;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class Cuenta {
	
	static ArrayList<Double> operaciones = new ArrayList<Double>();
	
	
	public Cuenta() {
		
	}
	
	public void depositar (Double monto)
	{
		operaciones.add(monto);
	}
	
	public void retirar (Double monto)
	{
		//if Verificar si se puede ejecutar el retiro
		operaciones.add(monto*-1);
	}
	
	public void movimientos() {
		
		Iterator<Double> iter = operaciones.iterator();
		System.out.println("\nOperaciones : ");
		Double valor;
		while (iter.hasNext()) {
			valor = iter.next();
		  if(valor>0){
			  System.out.println(String.format(Locale.US, "DEPOSITO %,.2f", valor));
		  }else {
			  System.out.println(String.format(Locale.US, "RETIRO %,.2f", valor));
		  }
		 
		}
	}
	
	public void saldo() {
		
	}
	

}
