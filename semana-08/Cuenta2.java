package semana08;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Cuenta2 {
	
	//static PrintWriter oS;
	PrintWriter oS;
	
	public Cuenta2() throws FileNotFoundException {
		//oS = new PrintWriter("operaciones.csv");
		//this.oS = new PrintWriter("operaciones.csv");
		this.oS = new PrintWriter(new FileOutputStream(new File("operaciones.csv"), true)); 
		this.oS.flush();
		this.oS.close();
	}
	
	public synchronized void depositar (Double monto) throws FileNotFoundException //synchronized
	{
		 this.oS = new PrintWriter(new FileOutputStream(new File("operaciones.csv"), true)); 

		//oS.println(monto);
		//oS.close();
		 
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		//System.out.println(dateFormat.format(date));
		 
		this.oS.println(monto +";"+dateFormat.format(date));
		this.oS.flush();
		this.oS.close();
		notify();
	}
	
	public synchronized void retirar (Double monto) throws FileNotFoundException //synchronized
	{
		 this.oS = new PrintWriter(new FileOutputStream(new File("operaciones.csv"), true)); 

		//oS.println(monto*-1);
		//oS.close();
		 
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		this.oS.println(monto*-1+";"+dateFormat.format(date));
		this.oS.flush();
		this.oS.close();
		notify();

	}
	
	public void movimientos() {
		
		
	}
	
	public void saldo() {
		
	}
	

}
