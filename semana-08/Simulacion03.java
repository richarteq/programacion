package semana08;

import java.util.Random;
import java.io.FileNotFoundException;
import java.util.Locale;

public class Simulacion03 {

	public static void main(String[] args) throws FileNotFoundException {
			
		Cuenta2 c01 = new Cuenta2();
		Random cantidad=new Random();
		
		for(int i=1;i<=10;i++) {
			Double monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
			c01.depositar(monto);
			System.out.println(String.format(Locale.US, "Se acaba de depositar %,.2f", monto));		
			
		}
		
		for(int i=1;i<=10;i++) {
			Double monto = (Double) (cantidad.nextDouble() * (cantidad.nextInt(2500)+1));
			c01.retirar(monto);
			System.out.println(String.format(Locale.US, "Se acaba de retirar %,.2f", monto*-1));		
			
		}
		
		c01.movimientos();
		

	}

}
