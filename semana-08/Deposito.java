package semana08;

import java.util.Locale;

public class Deposito extends Thread {

	Cuenta cuenta;
	Double monto;
	
	public Deposito(Cuenta c, Double m) {
		this.cuenta = c;
		this.monto = m;
	}
	
	public void run() {
		
		try{
			this.cuenta.depositar(monto);
			System.out.println(String.format(Locale.US, "%s acaba de depositar %,.2f", Thread.currentThread().getName(), monto));
		}catch(Exception e){
			System.out.println(e);
		}
		
	}
}
