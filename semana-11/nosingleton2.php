<?php
class Singleton
{
    // Contenedor Instancia de la clase
    public $instance = NULL;
   
    // Constructor privado, previene la creación de objetos vía new
    //private function __construct() { }

    // Clone no permitido
    //private function __clone() { }

    // Método singleton 
    public function getInstance()
    {
        if (is_null($this->instance)) {
            $this->instance = new Singleton();
        }

        return $this->instance;
    }
}

/**
 * The client code.
 */
function clientCode()
{
    $s1 = new Singleton();
    $s2 = new Singleton();
    if ($s1->getInstance() === $s2->getInstance()) {
        echo "Singleton works, both variables contain the same instance.";
    } else {
        echo "Singleton failed, variables contain different instances.";
    }
}

clientCode();

?>
